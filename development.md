# Embedded Development 

  * [First Steps in ARM Development](https://medium.com/gpio/first-steps-in-arm-development-3d9639de73cc)
  * [Great StackOverflow Answer on Bare Metal Intel](https://stackoverflow.com/questions/22054578/how-to-run-a-program-without-an-operating-system)
      * [Emulator mentioned there in](https://www.qemu.org/)

## Projects

  * [[NeoVim#Plugins]]

## Interfaces

  * [Apple USB Device Interface Guide](https://developer.apple.com/library/archive/documentation/DeviceDrivers/Conceptual/USBBook/USBIntro/USBIntro.html#//apple_ref/doc/uid/TP40000973)


## Robotics

  [Embedded Robotics: Robotics for Beginners](https://www.embedded-robotics.com/robotics-for-beginners/?amp=1)

## Platformio

  * [Setup](https://docs.platformio.org/en/latest//core/quickstart.html)
  * [Examples](https://github.com/platformio/platformio-examples/tree/develop/wiring-blink)
  * [IOTA](https://legacy.docs.iota.org/docs/iot/0.1/platformio/introduction/get-started)

## TI MSP43x
  * MSP432
    * [Hackster,io Tutorial](https://www.hackster.io/measley2/easy-embedded-systems-learning-with-ti-msp432-f9ea86)
    * [SimpleLink MSP432 SDK QuickStart](http://software-dl.ti.com/simplelink/esd/simplelink_msp432_sdk/1.40.00.28/docs/simplelink_mcu_sdk/Quick_Start_Guide.html) 
