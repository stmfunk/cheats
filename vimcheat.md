# Vim
Buffer                                                                      :       Vims representation of a file in memory.

# Core
.                                                                           :       Repeat last change to file whatever it was.
N<cmd>                                                                      :       Perform N cmd times


## File Management
:w[rite]                                                                    :       Write to file (Save)
:w !sudo tee % > /dev/null                                                  :       Write file as super user (hacky).

:!mkdir -p %:h                                                              :       Make the full path for a file in a virtual directory.

sav[eas]                                                                    :       Save to new filename and change the current filename to the new one.
                                                                                        {GAMECHANGER}

find                                                                        :       Find the matching file in the search path and open it.
set path                                                                    :       Configure the path used by the find command for file search.


netrw                                                                       :       Vims builtin file manager plugin
:e.
:[E]xplore                                                                  :       Open netrw window in current buffer
:Sexplore                                                                   :       Open netrw in a split window
:Vexplore                                                                   :       Open netrw in a vertical split window

## Informational
ga                                                                          :       Give the code of the character under the cursor.

## Operators
RULES                                                                       :       Operators operate on the motion they are given or if they are
                                                                                        repeated then the current line e.g dd delete current line.
c                                                                           :       Change command replaces the motion and puts you in insert mode.
C                                                                           :       Change til the end of the line.
s                                                                           :       Substitute, delete the character under cursor and enter insert.
S                                                                           :       Subsitute entire line from the beginning.
y
d                                                                           :       These are some of the 'operators'. Operators are defined by the
                                                                                        motion they are given.
p                                                                           :       Put contents of selected register in buffer. 
[p                                                                          :       Put contents of register in but adjust indentation.
g[x]                                                                        :       Modifies command. Operators in this class when doubled for single line
                                                                                            have shorthand where g is ommitted from repitition
                                                                                        ~ To swap case over motion.
                                                                                        u To make all lowercase over motion
                                                                                        U To make all uppercase over motion
!                                                                           :       FLAG FOR RESEARCH: filter motion data through external program.
=                                                                           :       Autoindent.

>                                                                           :       Increase tab for motion line
<                                                                           :       Decrease tab for motion lines

## Text Objects
i{x}                                                                        :       Delete inside of a text object 
a{x}                                                                        :       Delete around a text object 
                                                                                        {x} for many objects are the symbol itself.
ap                                                                          :       Operate on a paragraph (text object).
as                                                                          :       Operate on a sentence (text object).
aw                                                                          :       Word (text object).
daw                                                                         :       delete a word.
w                                                                           :       Move to start of next word (string of digits, letters and underscores, 
                                                                                        or any sequence of nonblank characters not include the former)
W                                                                           :       Move to start of next WORD (any string of characters followed by a whitespace).
b,B                                                                         :       Reverse of above.
e,E                                                                         :       Equivalent to w but to the end of a word
ge,gE                                                                       :       Equivalent to b but for e


## Jumps 
:jumps                                                                      :       Show jump list.
:changes                                                                    :       Show change list.
<C-o>                                                                       :       Traverse back through the jump list.
<C-i>                                                                       :       Traverse forward through the jump list.
g;
g,                                                                          :       Jump forward and backwards between change list locations.

H/M/L                                                                       :       Jump to top, middle or bottom of screen.
gf                                                                          :       Jump to file name under cursor.
<C-]>                                                                       :       Jump to definition of keyword under cursor.

`.                                                                          :       Marker is always set to the last change made.
`^                                                                          :       Marker of last end of insert mode.

''                                                                          :       Go to last cursor location line
``                                                                          :       Go exact last cursor location.
`                                                                           :       Move to exact location of mark
'                                                                           :       Move to start of markers line.


m{x}                                                                        :       Set a alphabetic character {x} as a marker.
                                                                                        x: Lowercase to set local file marker
                                                                                        X: Uppercase to set persistant interfile marker
'{x}                                                                        :       Move to the start of the line with marker {x}
`{x}                                                                        :       Move to the exact location of marker {x}

Special Makers                                                              :       `` : position of last jump in current file
                                                                                        `. : Location of last change
                                                                                        `^ : Location of last insertion
                                                                                        `[ : Start of last change or yank
                                                                                        `] : End of last change or yank
                                                                                        `< : Start of last visual selection
                                                                                        `> : End of last visual selection


at                                                                          :       Delete around xml tag object


## Movement
*                                                                           :       Go to next occurence of word under cursor
#                                                                           :       Go to previous occurence of word under cursor

zz                                                                          :       Center current line
zt                                                                          :       Move the current line to top of the window
zb                                                                          :       Move the current line to bottom of the window

zs                                                                          :       Move screen to the left so cursor is at start of screen. Allows
                                                                                        you to view ends of lines.
ze                                                                          :       Move window so start of line is on the edge

## Registers
"                                                                           :       Character for registers. Store data to copy and paste.
"=                                                                          :       Expression register (see below)


## Numeric
<C-a>                       
<C-x>                                                                       :       Increment/Decrement number under cursor. Will jump to the next 
                                                                                        number in the line if there is not one under the cursor
                                                                                        and it can be give a count. Also works with  negative numbers.
"=                                                                          :       The expression register, performs arithmetic on the values placed in it.

## Undo
u                                                                           :       Undo
<C-r>                                                                       :       Redo
M
U                                                                           :       Undo all changes made to last line that was edited

# Macros
@@                                                                          :       Execute last run macro.
@:                                                                          :       Execute last run ex command.
                                                                                        commands are saved as macros
@x                                                                          :       Run macro stored in register x

# Normal Mode
## Entering Modes
R                                                                           :       Enter Replace mode
gR                                                                          :       Enter virtual replace mode In virtual replace mode tabs are treated
                                                                                        as though they are a string of spaces.
r                                                                           :       Single shot replace mode.
gr                                                                          :       Single shot virtual replace mode.
i                                                                           :       Insert mode.
q:                                                                          :       Open the command-line window with history of commands
q/                                                                          :       Open the command-line window with history of searches

## Yanking
P                                                                           :       Put before cursor

## Searching
k
?for                                                                        :       Search in the opposite direction of /
/for                                                                        :       Search for 'for'
n                                                                           :       Next occurence of the searched for term.
n                                                                           :       Next occurence of the searched for term.
N                                                                           :       Go back to previous occurence.

:set hls(earch)                                                             :       Highlight search terms
:noh                                                                        :       Wipe the search highlighting on screen

fx                                                                          :       Go to next occurence of x in line.
Fx                                                                          :       Go to previous occurence of x in line.
;                                                                           :       Repeat the last f search.
,                                                                           :       Reverse last f search.
tx                                                                          :       Same as f but one char before.

*                                                                           :       Execute a search for that word under the cursor.


## Advanced Features
<C-p>                                                                       :       Completes word to closest match.
<C-n>                                                                       :       Completes word to first alphabetical match

:ab y Yord                                                                  :       Abberiviate Yord as y
:una y                                                                      :       To unset y


## Windowing
viewports                                                                   :       Viewports allow you to view/edit multiple parts of the same file
                                                                                        at the same time. View ports are basically buffers.
                                                                                        You can use pane manipulation shortcuts to manipulate viewports.
:sp {file}                                                                  :       split horizontally, <C-w> s
:vsp {file}                                                                 :       split vertically, <C-w> v
<C-w>c      
:on[ly]                                                                     :       Close all windows but this one.
<C-w>o
:clo[se]                                                                    :       Close active window.


<C-w>                                                                       :       Enter window command
<C-w> s                                                                     :       Split window horizontally
<C-w> v                                                                     :       Split window vertically
<C-w> q                                                                     :       Close focused window pane
<C-w> d                                                                     :       Move in direction key d
<C-w> n+/-                                                                  :       Decrease/increase window size by n
<C-w> n</>                                                                  :       Same principle but for vertical format.
<C-w> _                                                                     :       Minimise all other splits
[N]<C-w> _                                                                  :       Set active window height to N rows. 
[N]<C-w> |                                                                  :       Set active window width to N columns. 
<C-w> =                                                                     :       Equalise all splits
[N]<C-w> _                                                                  :       Set width of current window to N cols
[N]<C-w> =                                                                  :       Set height of current window to N cols

<C-w> Shift-T                                                               :       Move current pane to new tab.
<C-w> r                                                                     :       Rotate viewports clockwise
<C-w> R                                                                     :       Rotate viewports anticlockwise

## Buffers
<C-6>                                                                       :       Switch buffers


## Tabs
:tabc                                                                       :       Close a tab

# Visual Mode
v                                                                           :       Toggle normal visual mode. Visual mode acts on a character range
                                                                                        it will not remember what selection operator waS USED.
V                                                                           :       Toggle linewise visual mode.
<C-v>                                                                       :       Visual Block mode
<C-g>                                                                       :       Enter select mode, which overwrites selection. (Similiar
                                                                                        to using c)

va<obj>                                                                     :       Select object zone visually inclusively.
vi<obj>                                                                     :       Select object zone visually exclusively.

gv                                                                          :       Reselect most recently selected text.
o                                                                           :       Move cursor to opposite side of text selection.

c                                                                           :       Replace selection enter insert mode.
A                                                                           :       Append after visual selection.
U                                                                           :       Change selection to uppercase.
u                                                                           :       Change selection to lowercase

## Visual Block Mode
<C-v>                                                                       :       Enter visual block mode. Allows editing tabular data.
$                                                                           :       Extend selection to end of each line, (ragged visual block)


# Options
number                                                                      :       Line numbering option
showcmd                                                                     :       Display the command in normal mode as it is entered.

nrformats                                                                   :       Choose how vim interprets numbers (by default leading zeroes
                                                                                        mean octal, etc).


:set opt!                                                                   :       Toggle option

list                                                                        :       Show invisibles (input specific characters for tab and newline)
wildmode                                                                    :       Specify tab completion behaviour.
wildmenu                                                                    :       Enable the navigatable autocomplete menu
spell                                                                       :       Turn on spell checker.
                                                                                        zg : Add a word to the spellfile

## Tabs
tabstop                                                                     :       Tell vim how many columns a tab counts for.
expandtab                                                                   :       Tabs are converted to spaces.
shiftwidth                                                                  :       How many columns text is indented with reindent operators (>>) 
                                                                                        and automatic indentation.

## Substitution
s/fg/&c/                                                                    :       To add c to all occurences of fg.

# Mapping
<silent>                                                                    :       Sets it so that you do nnot see a command being executed.
                                                                                        (doesn't print the command being executed)
:nmap <silent> <<C-x>> exec                                                 :       Perform exec keystrokes when shortcut pressed
                                                                                        <CR>  :  For carraige return
                                                                                        <Esc> :  For escape
:omap                                                                       :       Used to define custom motion objects.
                                                                                        omap-info

# Ex Mode
:[range]cmd[reg] {address}                                                  :       Command on range of lines storing in reg or directed at address.
Range                                                                       :       The first argument is always the address of the action.
                                                                                        If we type a number it is interpreted as a line number and the
                                                                                        cursor is moved to that line.
Shortcuts                                                                   :       Shortcuts available in insert mode are usable here to.
:retab                                                                      :       Convert all tabs in document to tabs used in vim.
!                                                                           :       Override command modifier when placed directly after the command.
:shell                                                                      :       Enter an interactive shell.
<C-z>                                                                       :       Suspend vim to background
fg                                                                          :       Restore suspended vim job

@:                                                                          :       Repeat last Ex command.
&                                                                           :       Repeat last search and replace command.
$                                                                           :       Represents the end of the file.
%                                                                           :       Shorthand for the current filename
.                                                                           :       Represents the address of the line the cursor is one.
0                                                                           :       Virtual line that represents the line before the first in the file.

/{pat}/                                                                     :       Specify address by pattern {pat},
{addr}+n                                                                    :       Specify address as n lines below addr.
{addr}-n                                                                    :       Specify address as n lines above addr.
<Tab>                                                                       :       Tab completion
<C-d>                                                                       :       List autocomplete options.
<C-o>                                                                       :       Traverse back through the jump list.
<C-i>                                                                       :       Traverse forward through the jump list.
<C-r><C-w>                                                                  :       Put the word under cursor into command prompt
<C-r><C-a>                                                                  :       Put WORD (pending clarification).

'<'>                                                                       :       Specify the beginning and end of the most recent visual selection command mode.
q:                                                                          :       Open the command-line window with history of commands
                                                                                        <CR> to execute current line of the cmdwin
q/                                                                          :       Open the command-line window with history of searches
<C-f>                                                                       :       Switch from command mode to command line window.

:shell                                                                      :       Start an interactive shell in vim

delete                                                                      :       Delete lines into register x.
d
yank                                                                        :       Yank lines into register x.

copy                                                                        :       Copy line range to address
t
co
move                                                                        :       Move line range to address.
m
join                                                                        :       Join lines in range
normal {commands}                                                           :       Execute normal mode commands on specified lines.
substitute                                                                  :       Search and replace
s
global                                                                      :       Execute a command on lines in range that match the pattern.
g

\`\`                                                                          :       Vim will execute the command enclosed in the shell and pass it 
                                                                                        as an argument to the command given.

## Useful Commands
:Man																		: 		Open linux man page in new buffer like help page.

## External Commands
read !cmd                                                                   :       Read cmd output into buffer.
write !cmd                                                                  :       Write file to input of cmd
n,m!cmd                                                                     :       Pass lines n,m into cmd and replace them in file with the output

## Text Commands
:[range] cmd {params}                                                       :       Perform a command on a range of text. Params can be a buffer, a command etc.
                                                                                        If no range is given the cmd is executed on the line the cursor is currently
                                                                                        on.
                                                                                        range: Range can be given as a line number, a marker or a pattern.
delete                                                                      :       Delete range into buffer param.
yank                                                                        :       Yank range into buffer param.
[t][co]py                                                                   :       Copy lines in range to line address param.
                                                                                        t: shorthand
[m]ove                                                                      :       Move lines in range to line address param.
join                                                                        :       Join lines in range to single line.
normal                                                                      :       Execute normal command param on text range.
                                                                                        normal .  :    Execute the . command on range.
[s]ubstitute                                                                :       Substitute command (syntax elsewhere)
global                                                                      :       Global command (syntax elsewhere)

[r]ead                                                                      :       Insert the standard output of the file into the buffer.
[w]rite                                                                     :       Use the contents of the file in standard input.
[e]dit                                                                      :       Load a file into a new buffer.
e!                                                                          :       

source                                                                      :       Execute a vim batch script.

## Buffer Commands
ls                                                                          :       List buffer loaded.
<C-^>                                                                       :       Switch between current buffer and alternate.

bufdo                                                                       :       Perform a command on buffer list.
[b]uffer {name/n}                                                           :       Go to buffer name


bprev                                                                       :       Last buffer.
bnext                                                                       :       Next buffer.
[bf]irst                                                                    :       Go to first buffer in list.
[bl]ast                                                                     :       Go to last buffer in list.
[bd]elete {n1 n2 n3}                                                        :       Delete buffer (optionally give names numbers)
n,m bd                                                                      :       Delete buffer range n to m

args                                                                        :       Display the argument list. This is an editable list of files.
                                                                                        [file] is the active file
args {args}                                                                 :       Populate the argument list. Args can be anythint
argdo                                                                       :       Perform a command on each vim argument.

**                                                                          :       Expression matches directories recursively.
next,prev                                                                   :       Navigate through buffer list.


# Insert Mode
<C-h>                                                                       :       Delete back one character.
<C-w>                                                                       :       Delete back one word.
<C-u>                                                                       :       Delete back to start of the line.
ESC 
<C-[>                                                                       :       Shortcut for go back to normal mode.

<C-o>                                                                       :       Insert normal mode. Execute one command then return to
                                                                                        insert.
<C-r>[n]                                                                    :       Insert data from register n.
<C-r> <C-p>                                                                 :       Smarter version of C-r that fixes indentation.
<C-r>=                                                                      :       Access data in the expression register (i.e. type the expression 
                                                                                      you wish to evaluate at the bottom of the screen and the evaluated
                                                                                      expression will be inserted).

<C-v>0xx                                                                    :       Use to enter a character by its ascii code (prefaced by a 0 always)
<C-v>uxxxx                                                                  :       Use to enter a character by its unicode (prefaced by a u always)
<C-v><Tab>                                                                  :       Insert a tab character even if expandtab is set.
<C-k>{x}{x}                                                                 :       Insert character by digraph. 
                                                                                        default digraph conventions summarized :h digraph-default
                                                                                        list of available digraphs :digraph
                                                                                        more user friendly list :h digraph-table


## Coloration
hi Comment ctermfg=color                                                    :       Change the colour for comment.


## Modelines
vim: set etc:                                                               :       Allows you to set file specific settings.


# Extensions/Plugins
## Installing
To install a single file plugin place it in the .vim/plugin directory and it will be autoloaded.

## Useful Plugins
commentary.vim                                                              :       Adds a comment adding operator.
                                                                                        https://github.com/tpope/vim-commentary

Commentary.vim

PDFs																		:		Would like to work with PDFS in vim.
																						[Vim PDF tools](https://dev.to/l04db4l4nc3r/vim-to-the-rescue-pdf-preview-2e10)
																						[Sioyek PDF Viewer](https://github.com/ahrm/sioyek)



## surround.vim
csxz                                                                        :       Replace surrounding xs with zs. (Only works on delimiters and tags)
dsx                                                                         :       Delete surrounding delimiters x.
ysiwx                                                                       :       Surround word text object with delimiter x. 


## Tricks
:read !cmd                                                                  :       Read the contents of the command into the current vim buffer

:checkhealth                                                                :       neovim status

:%!xmllint --format -                                                       :       Use xmllint to format the current buffer in xml.
:vimgrepa `/\(if.*\)\@<!g_intruder\.state.* = /g *.[ch]`                      :       Search for g_instruder.state without a preceding if clause. 
                                                                                        @<\! is a reverse lookup modifier.

## Extensions to check out

   - [ ]  [marks.nvim](https://github.com/chentoast/marks.nvim)
   - [ ]  [vim-bookmarks](https://github.com/MattesGroeger/vim-bookmarks)
   - [ ]  [obsidian.nvim](https://github.com/epwalsh/obsidian.nvim)

