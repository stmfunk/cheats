## Basics
Primitives are objects

args list contains the command line arguments.

## Variables & Types
val                             :       Declare an immutable variable.
val n                           :       To declare a variable.
val n = 12                      :       Scala will infer the type.
val n: Int = 12                 :       Explicitly declare the type of the variable.

(1,x)                           :       Construct a tuple or pair. Typing same as haskell.
                                            _n references the property, e.g. p._2

List()                          :       To construct a list.
                                            ::      cons like in haskell
                                            Nil     the empty list
                                            .filter filter list with boolean function
                                            ++      append two lists
                                            .map    map function to list

var                             :       Create mutable variable.

## Flow
if () {}                        :       Control flow syntax similiar to java, but statements are expressions
                                            so they evaluate.
x match {}                      :       Switch case in scala, however can pattern match against other types
                                            case n => {}

for (i <- 0 until x)            :       For loop in scala

## Functions
def f(x: Type): Type = E        :       Declare a function. Function declarations and calls must include parentheses.
                                           The type of this function is Type => Type
                                           Function bodies can be encased in {} and contain multiple lines, seperated
                                             by semicolons (optional if seperation is sufficiently inferred).
return                          :       Optional way to specify return value, same as the value alone.

## Objects
Nil                             :       The empty list.
.toString                       :       Method shared by all objects, converts to string.

class                           :       Classes can be created to define new types.
abstract class                  :       Define a base for a new type, i.e. define your type as integer.
case class Xxx()                :       Define instances of your class i.e. populate your class with integers.
                                            extends C to specify the abstract class
class X(a,b)                    :       Classes can have arguments.
object Y(c, d)                  :       An object that has only one instance.
case object X                   :       For classes that have only one object.

extends                         :       Keyword for inheritance. use `override` to change specific fields.
with                            :       Used to assign a class with a trait.

trait                           :       Defines a trait that can be assigned to an object. A trait is set of 
                                        methods and fields with implentations.


this()                          :       Can be used to create multiple constructors, for object overloading.

## IO
println("")                     :       To print a string.

## Interpreter Instructions
:load                           :       Load script.
:t                              :       Give type of expression.

### Strings
"".size                         :       Return return size of string.

### Lists
.foreach{ x => }                :       Loop over the list with the item x holding the list item.

n until k (by x)                :       Specify a range to store as a Range object.
n to k                          :       Inclusive version of above.

### Tuples
(x,y)._n                        :       To specify and reference a tuple.
