# C
Start VM:
VBoxManage startvm "Embedded Software" --type headless


ctype.h             :           File containing isalnum(), isalpha(), iscntrl(), isdigit(), isgraph(),
                                islower(), isprint(), ispunct(), isspace(), isupper(), isxdigit(),
                                tolower(), toupper(), functions

string.h            :       All the strcmp stuff
stdlib              :       Contains rand().

## Types

double              :           More accurate than float.

## Data Allocation
const               :           Type qualifier allocates data as a constant and is mapped to Read-Only Memory.
                                  Will be placed in Code memory.
unsigned            :           Specifies the data is positive only. (Type modifier)
signed              :           Explicity specifies the data may be negative.
short               :           Specifies data type will only use 16 bits.
long                :           Specify data type will use 32 bits.
long long           :           Specify data type will use 64 bits.

auto                :           The variable should be automatically allocated and deallocated data on the stack
                                  Has a lifetime of a function or block.
static              :           The data should persist for the lifetime of the program.
                                  Can be stored in both .data or .bss.
                                  If defined inside a function, though it persists between executions it can
                                    only be accessed inside the function.
extern              :           Specify that a variable is defined in another file.
                                  Allows access of a variable defined in another C file's scope.
register            :           Allocate this variable directly to a CPU register. It can be used to speed up
                                  operations for a frequently used variable.
                                  Only a guideline and not guaranteed to be followed.
                                  Uncommon.

## Dynamic Memory
malloc              :           Allocates N contiguous bytes in Heap Space.
                                  Returns a void pointer type.
calloc              :           Allocates N contiguous bytes in Heap Space. Also initalize memspace to 0.
                                  Returns a void pointer type.
realloc             :           Change allocation size of memory size. May be in entirely new region.
free                :           Free allocated region.
sizeof              :           Return size of a data type.


# GNU GCC
## Compiler
-Werror             :           Stop compilation if a warning occurs. (i.e. treat warnings as errors).
-o                  :           Specify output file explicitly.
-std=c99            :           Specify compilation to adhere to c99 standard.
-Wall               :           All warning messages will be displayed (not as errors).
-v                  :           Verbose compiler output.
-c                  :           Compile and assemble but do not link (produce an object file).
-g                  :           Add debug symbols to assembly code.

-S                  :           Translate code in Assembly for human reading.
-E                  :           Halt build after preprocessing.

## ARM Cross Compiler
--specs=            :           Specify the specification file for compilation.
-mcpu=              :           Specify the target architecture.
-mthumb             :           Generate code in the Thumb states (ISA) (??)
-marm               :           Generate code in the ARM state (ISA) (??)
-mthumb-interwork   :           Generate code that supports calling between ARM and Thumb (ISA) (??)

-mlittle-endian     :           Generate code for Little Endian Mode
-mbig-endian        :           ""      ""     ""     ""

## Preprocessor
-D                  :           Define a macro at compile time. Allows you to trigger preprocessor directives dynamically.

\#define             :           Define a macro. Can be used to define constant, boolean flag or function macro.
\#undef              :           Undefine a macro definition previously declared;
\#ifdef              :           Include block if macro defined.
\#ifndef             :           Include block if macro not defined.
\#endif              :           End ifdef block.
\#include            :           Include headers files.
\#warning            :           Throw a warning.
\#error              :           Throw an error.
\#pragma             :           Specify compilation options.
                                  \#pragma once   Forces an include to only be used once per file.

## Linker
ld                  :           Manual command for linking an object file.
-map                :           Produce a map file to be reviewed by the programmer.
-T                  :           Specify a Linker file.
-nostdlib           :           Tell linker not to include stdlib.
-O<#>               :           Specify level of optimization (1-3)
-Os                 :           Optimize for memory size.
-z stacksize=\[N]    :           Specify stacksize of N.
-shared             :           Produce a shared library.
-l \[LIB]            :           Link with library.
-L \[DIR]            :           Include the library directory.
-Wl, \<OPT>          :           Pass option to linker from compiler.
-Xlinker \<OPT>      :           Pass option to linker from compiler.

## Other GNU Tools

size                :           Inspect size used by memory sections in object and compiled files.
                                  It is used to examine memory footprint.
nm                  :           Used to inspect symbols and their sizes in an object file.
objcopy             :           Used to copy and convert object files.
objdump             :           Dump information from object file. Can use to retrieve assembly code from object file.
readelf             :           Used to interpret ELF files. 
gdb

# Make
Makefile            :           Used to specify correct compiler instruction for the target.
all                 :           Make command that builds final executable.
.PHONY              :           Directive that prevents make from attempting to find a file matching target name.

target: d1 d2       :           Declare a build rule (executed using make target) requiring files d1 and d2.
include             :           Include another Makefile.
\# \                  :           Comment and split line.
VAR = ...           :           Declare a recursively expanded variable. (Variable is expanded upon each use).
VAR := ...          :           Declare a simply expanded variable. (Variable is expanded immediately).
$(VAR)              :           Reference a variable.
$@                  :           Automatic variable always refers to the target of the current recipe.
$^                  :           Automatic variable always refers to the prerequisites of the current recipe.
%                   :           Pattern match operator. Using this operator we can use make to automatically create recipes.
                                  %.o: %.c
                                  OBJS:=$(SRCS:.c=.o) Creates an OBJS list by replacing .c in source with .o for object files.
  
$(shell cmd)        :           Return output of shell command.
ifeq endif          :           Create an if statement in Make.
make VAR=x          :           Pass a variable into make from command line call.