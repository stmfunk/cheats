# Thailand

## Itinary 

- [x] Make map
- [x] Book party accomodation
- [ ] Koh Samui Accomodation
- [x] Koh Pha Ngan Accomodation
- [ ] Travel License
- [x] Vaccines
- [ ] Ferry Tickets
- [ ] New SIM

## Full Moon Party

  *  You can't book tickets in advance

## Links

   - [ ]  [Travel eSIM Nomad](https://www.getnomad.app/en/thailand-eSIM) 

## Accomodation

### Koh Pha Ngan

![[koh_pha_ngan_hotel.pdf]]

![[koh_pha_ngan_hotel_message.pdf]]

### Bangkok

![[bangkok_hotel_booking.pdf]]

## Flights

### Internal

![[bangkok_to_koh_samui_ticket_mairi.pdf]]

![[bangkok_to_koh_samui_ticket_dhonaill.pdf]]


### Long Haul

![[bangkok_flight_info.pdf]]

![[edinburgh_to_bangkok_flight.pdf]]

## International License 

![[AA_IDP-Application202310-1.pdf]]

