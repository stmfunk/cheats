# Python
## Base
getattr         			:           Returns reference to an attribute from an object, 
                			                when given an object and the string name of the attribute. 
## Libs
gitpython       			:           Library for manipulating git repositories.

## Object Features
`__repr__`                  :       Build a representation of object that is to used programmatically not read.

## Advanced Features
`*args`                     :       Allows the passage of variable numbers of arguments to a function.
`**kwargs`                  :       Allows the passing of previously unspecified named arguments to a function.

## Libs
gitpython                   :       Library for manipulating git repositories.

argparse

## iPython

## Decorators
Decorators                  :       Decorators are function wrappers in python. They essential take a function and return a composite of that
                                        that function with another.

`@decfunc`                  :       When placed before a function declaration will decorate the succeding function with decfun.
`@decfunc('arg')`           :       Pass arguments to the decorator function.
