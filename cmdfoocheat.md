# Command Line Kung-Fu
find . -name "file"         :       Find all files named file in this directory
                                        -iname  : case insensitive

lsusb                       :       List usb devices.

## Tools
watch                       :       Periodically execute a command display result fullscreen

ss                          :       Socket statistics,  

cut                         :       Line editing by columns
                                        -d'x': specify delimiter
                                        -fn,m: select columns n and m
tail                        :       Output end of the file.
                                        -f: wait for additional lines to be added to file and 
                                            output them.
                                        --user-agent: Set a browser detail headers for user agent to circumvent server permissions.
                                        ="Mozilla/5.0 (Windows NT x.y; rv:10.0) Gecko/20100101 Firefox/10.0"

tio 						:		Serial monitor for the command line.
minicom						:		Serial monitor for the command line. (BETTER!)


[[AWK]]						:		Powerful programming language for manipulating text at high speed.


## ls 

-lL 						:		Follow symbolic links.

## sed
-i							:		Edit file and save it back when done.
-f							:		Load from script file

### sed Archive
`0,/$Rev.*$/{s/\($Rev\):.*$:/\1$:/}`1 		:		Replace first occurence of $Rev: xxxxx+ $:  with $Rev$:
