# Electrical Engineering 

## Index 
- [[Tools]]


## Components 
  * [TI TLV775P Low Dropout Regulator](file:~/devStuff/Docs/datasheets/tlv755p.pdf)
  * [TI BoostXL Motor Driver (For Syringe Motor)](file:~/devStuff/Docs/datasheets/TI_BOOSTXL_User_Guide.pdf)
  * [IC DC Motor Driver](https://www.digikey.com/en/products/detail/stmicroelectronics/L298N/585918)

## For Reference 

  * [Connector Pinout Guide](https://www.vpi.us/technical-articles/technote-1788)
  * [Hackaday: The Bus Pirate, Universal Serial Interface](https://hackaday.com/2008/11/19/how-to-the-bus-pirate-universal-serial-interface/)
