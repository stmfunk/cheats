= Software Testing =

- [ ] [[https://www.pmi.org/pmbok-guide-standards/foundational/PMBOK|PMBOK]]


== Squish ==

  - [[https://doc.froglogic.com/squish/latest/index-noframes.html|Squish Manual]]
  - [[https://doc.froglogic.com/squish/latest/tutorials-win.html|Squish for Windows Tutorial]]


== Books ==

  - [[https://www.amazon.co.uk/Driven-Development-Embedded-Pragmatic-Programmers/dp/193435662X/ref=sr_1_13?dchild=1&keywords=embedded+testing&qid=1634050030&qsid=262-7968421-3297456&sr=8-13&sres=B07Q2T9ZVQ%2C1439818452%2C1439821402%2CB08L8HT1KT%2C3330331178%2C1441953914%2CB084VD45S1%2CB0855V3N57%2C3659813249%2C1789619130%2C1787280578%2C3958458165%2C193435662X%2CB09J871B11%2C178934056X%2C1484264398&srpt=ABIS_BOOK|Pragmatic Programmers: Test Driven Development for Embedded C]]
  - [[https://www.softwaretestinghelp.com/software-testing-books/|Software Testing Book Lists]]
  - [[https://www.amazon.co.uk/Testing-Complex-Embedded-Systems-Pries-dp-1439821402/dp/1439821402/ref=mt_other?_encoding=UTF8&me=&qid=|Testing Complex and Embedded Systems]]
  - [[https://bookauthority.org/books/best-software-integration-testing-books|List of Integration Test Books]]
