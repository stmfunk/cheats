# Tools

  * [[grepcheat|Grep/Egrep]]
  * [[gitcheats|Git]]
  * [[tarcheat|Tar]]
  * [[tmuxcheat|Tmux]]
  * [[jekyllcheat|Jekyll]]
  * [[minicomcheat|Minicom]]

  - [ ] [[zathura|Zathura]]

## Tee
ls -al | tee txt                    :       Pipe output to tee, which will copy 
                                                output to screen and save it in a file

dot                                 :       Make graphs


## find
find dir -name file.nme             :       Find the file with matching name in dir.
find dir -name '*.pdf'              :       Globs need to be quoted otherwise bash will expand.
find dir -type x file               :       Specify the type of the file. Will only match files of selected type
                                                f : ordinary file
                                                d : directory
                                                l : symbolic link
                                                s : socket
                                                p : FIFO
                                                (more)
find dir -empty                     :       Match empty files.
find dir -user uname                :       Find files owned by user.
-mtime n                            :       Files modified in the last n days.
-iname                              :       Case insensitive.

## sort
sorts input

-t' '                               :       Specify delimeter
-kn                                 :       Tell sort to sort on the nth column

## Code Analysis
ctags					:			-R, --extra=+fq --kinds-C=+l
Piped trace receive		:			stdbuf -o0 ncat -ul 51002 | stdbuf -o0 fold | sed "s/\[01/\&\[01/g" -u | stdbuf -o0 tr -d '\n' | stdbuf -o0 tr "&" "\n" | stdbuf -o0 grep "SERIAL: RX" | stdbuf -o0 cut -d']' -f4 | stdbuf -o0 cut -d' ' -f4- | xxd -p -r



## tio

-b 						:			Baud option.
