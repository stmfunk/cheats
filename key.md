# Key
====                    :       Main heading
----                    :       Subheading.

x : t                   :       Syntax explanation
blahblahblah            :       Detailed concept explanation.
*!*                     :       Important item.
#?n#                    :       Tip n
*                       :       Bullet point
...                     :       Awaiting information.
>> n                    :       Tag chapter or section.
""                      :       Quotes
[lnk]                   :       Hyperlink
