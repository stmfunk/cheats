# [BG95 Article](https://docs.monogoto.io/getting-started/general-device-configurations/iot-devices/quectel-bg95-mx)

### Quectel BG95-MX
#### Set up the Quectel BG95 cellular modem
The Quectel BG95-M series support LTE Cat M1 technology. The M3 and M5 also come with GSM/EDGE support. This tutorial highlights the  which supports LTE Cat M1, NB-IoT2 and EGPRS and is compliant with 3GPP release 14.

#### Set up the EVB Kit
Connect the Quectel BG95-M3 modem to the EVB kit. 
Connect the LTE antenna and (optionally) GPS antenna
Insert the Monogoto SIM
Connect you PC to the EVB Kit
Use the USB to UART converter cable to connect your computer to the COM1 MAIN port of the EVB Kit
When using Windows, install the driver using the disk or USB stick provided by Quectel.
Connect to the EVB kit using a terminal program. You can use PuTTY for Windows, or screen, minicom or miniterm for Mac or Linux.
Power the EVB Kit using the USB to micro USB cable and switch the POWER button to ON. The POWER LED will turn on RED. 
Press and hold the PWRKEY button for 1 second, the STATUS light will light up on GREEN, the NET_STA LED will start blinking BLUE.
Once the modem has started, it will report:
```
RDY
APP RDY
```
Enter AT, if the connection with the BG95 modem has been established, the board will answer with OK.

#### Connect the BG95 to Monogoto
Keep the AT Commands manual next to you for more details about the commands. here to download the latest version.
Reset the modem to its default configuration:
Reset non-volatile memory:
```
AT+QPRTPARA=1 
```
Restore factory settings:
```
AT+QPRTPARA=3
```
Reboot module:
```
AT+CFUN=1,1
```
Set the error reporting to verbose, resulting in more descriptive error messages (optional):
```
AT+CMEE=2
```
Check the status of the SIM card. The correct response should be: +CPIN: READY
```
AT+CPIN?
```
If you receive an error, the SIM may not be inserted properly or the SIM is protected with a PIN. By default, Monogoto SIMs are not PIN protected.
Query device information
Request the product type, hardware and firmware version:
```
ATI
```
Request the IMSI (International Mobile Subscriber Identity):
```
AT+CIMI
```
Request the ICCID (Integrated Circuit Card ID), which is the identification number of the SIM card
```
AT+CCID
```

#### Network Configuration
It is possible to let the modem select the network automatically, or to manually define a network to connect with.
kk##### Option 1: Automatic Network Selection
To set the modem to automatic network selection enter the command:
```
AT+COPS=0
```
Check the network and cellular technology the modem is currently using:
```
AT+COPS? 
```
Expected response: +COPS: 0,0,"\<name of operator>",X. The last digit indicates the cellular technology:

> 0 indicates GSM
> 8 indicates LTE Cat-M1 (also referred to as eMTC)
> 9 indicates NB-IoT

##### Option 2: Manual Network Selection
Start with searching for available networks:
AT+COPS=?
It may take several minutes before the modem responds. 
The modem responds with the names and the MCCMNC codes of the available networks. It also shows the cellular technologies the networks support: 
```
+COPS: (1,"vodafone NL","voda NL","20404",8),(1,"NL KPN","NL KPN","20408",0),(1,"vodafone NL","voda NL","20404",9),(1,"T-Mobile NL","TMO NL","20416",0),(1,"NL KPN","NL KPN","20408",8),(1,"vodafone NL","voda NL","20404",0),(1,"T-Mobile NL","TMO NL","20416",8),(1,"T-Mobile NL","TMO NL","20416",9),,(0,1,2,3,4),(0,1,2)

OK
```
To find which of the networks Monogoto has roaming agreements with, review the .
To find the profile of your SIM, visit the , open the page Things  and select a specific Thing. Scroll down to Mobile Identities to find your profile in the column Network Provider Name.

Check the network and cellular technology the modem is currently using:
```
AT+COPS? 
```
Expected response: +COPS: 0,0,"\<name of operator>",X. The last digit indicates the cellular technology.
Manually set the network and the cellular technology:
```
AT+COPS=1,2,"XXXXX",8
```
Replace XXXXX with the MCCMNC code of your operator, for example"20404" for Vodafone NL. Use 8 for LTE Cat-M1.
Check the radio signal strength and signal quality:
```
AT+CSQ
```

+CSQ returns 2 values separated by a comma. The first value represents the signal strength and provides a value between 0 and 31; higher numbers indicate better signal strength. The second value represents the signal quality indicated by a value between 0 and 7. If AT+CSQ returns 99,99, the signal is undetectable or unknown.
#### Define the cellular band(s) (optional)
It is possible to lock the modem to one or more specific bands. This may result in a faster boot cycle as the modem does not need to scan for available bands at startup.
Supported LTE-M bands for the BG95-M1, -M2, -M3, -M5 and -MF are: 1 2 3 4 5 8 12 13 18 19 20 25 26 27 28 66 85
The BG95-M4 supports in addition to the bands mentioned above: 31 72 73
Although the modem may support many different bands, your local network operator may not. Have a look at the  to find the available band(s) in your region.
To lock the modem to a specific band, a hexadecimal value representing the band needs to be sent to the modem. To lock the modem to LTE-M band 20, send: 
```
AT+QCFG="band",0,0x80000,0
```

For information about setting specific LTE bands, see , chapter 3.1.1.4.

To test if the band(s) is properly set, enter the command: 
```
AT+QCFG="band"
```

> Example response: +QCFG: "band",0xf,0x80000,0x10004200000000090e189f

The first hex number represents the GSM bands, followed by the LTE-M bands and NB-IoT bands.
Query Network information
To information about the technology used, the network provider and cellular band, enter:
```
AT+QNWINFO
```

> Example response: +QNWINFO: "eMTC","20408","LTE BAND 20",6400. 

Configure Radio Access Technology (RAT)
Configure the modem to only use LTE Cat-M1, not NB-IoT:
```
AT+QCFG="iotopmode",0,1
```

The last variable: 1, instructs the modem to process the command immediately. When using: 0, the takes effect after a reboot. 
Specify the modem search sequence starting with LTE Cat-M1 (indicated with 02) followed by GSM (indicated with 01) and NB-IoT (03): 
```
AT+QCFG="nwscanseq",020103,1
```

#### Network Activation
Set the APN to data.mono:
```
AT+CGDCONT=1,"IP","data.mono"
```

Validate if the APN is set correctly:
```
AT+CGDCONT?
```

> Expected response: +CGDCONT: 1,"IP","data.mono","0.0.0.0",0,0,0,0,0,0

Activate the PDP (packet data protocol) context:
```
AT+CGACT=1,1
```

Test if the PDP context is activated:
```
AT+CGACT?
```

> Expected response: +CGACT: 1,1

Validate if your device received an IP address:

```
AT+CGPADDR
```

> Expected response: +CGPADDR: 1,XX.XXX.XX.XXX

Do you see an IP address? Congratulations! You’ve successfully connected the Quectel modem to Monogoto 🎉

#### Test the connection by sending a PING
A PING test can be performed to test if the modem has an active data connection with a mobile network.
When cellular modems are idle for a long period of time, cell towers might drop the data connection to save resources. Sending regular PINGs is a good method for testing the data connection, as well as for keeping the connection alive because the operator registers your device as being actively used.
Send 5 PINGs to IP address 8.8.8.8:
```
AT+QPING=1,"8.8.8.8",5
```

Example response:
> +QPING: 0,"8.8.8.8",32,543,255
> +QPING: 0,"8.8.8.8",32,396,255
> +QPING: 0,"8.8.8.8",32,262,255
> +QPING: 0,"8.8.8.8",32,299,255
> +QPING: 0,4,4,0,262,543,375

Great work on connecting the Quectel BG95 to Monogoto! Have a look at the Things logs in the  to find more details about the established connection.

#### Troubleshooting 
Network reactivation
When cellular modems are idle for a long period of time, cell towers might drop the data connection to save resources. 
To test if the modes still have an active session, the command AT+CSQ can be sent. If no value or the value 99 is returned, the device lost its session with the cell tower and the modem needs to be re-initialized. This can be done by rebooting the modem using the command AT+CFUN=1,1
