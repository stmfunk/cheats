# General Programming

  * [[agentspeakcheat|AgentSpeak]]
  * [[ccheat|C]]
  * [[haskellcheat|Haskell]]
  * [[matlabcheat|Matlab]]
  * [[mipscheat|MIPS]]
  * [[prologcheat|Prolog]]
  * [[pythoncheat|Python]]
  * [[rubycheat|Ruby]]
  * [[scalacheat|Scala]]

## Think Pieces

  * [About C](https://gankra.github.io/blah/c-isnt-a-language/)
  * [The Overflow: Dynamic Programming](https://stackoverflow.blog/2022/01/31/the-complete-beginners-guide-to-dynamic-programming/)
