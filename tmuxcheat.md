Splith      :       "
Splitv      :       %

Mod+ Alt+direction          :       Resize pane up or down.

Mode + Alt + Space          :       Change paneling layout.

swap-window                 :       Swap window.
                                        -t n : Swap the current window to window n


## Panes
Mod + !                     :       Break pane out of window.
join-pane                   :       Join a pane to the current window.

## Sessions
Mod + s                     :       List sessions available
tmux ls
:ls                         :       Same as above.

tmux new -s name            :       Start a new session called name.
tmux kill-session -t name   :       Destroy a session.

