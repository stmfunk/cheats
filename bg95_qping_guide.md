---
created: 2023-12-05T17:36:18 (UTC +00:00)
tags: [quectel,bg95]
source: https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/
author: 
---

# [How to ping your modem using AT commands | Onomondo](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/)

> ## Excerpt
> Checking your modem’s data connection using a ping test AT command after network attachment can help debug connectivity issues for IoT devices. Here's how.

---
Checking the data connection after a successful network attachment can help debug issues with connectivity. 

Performing a ping test with your modem is a good way to test connectivity. However, there is no uniform ping test AT command.

In this guide, we’ll go through how to ping Google’s primary DNS server (the server with the IP 8.8.8.8) with these modems:

-   [Quectel’s BG96](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#Quectel-BG96)
-   [SIMCom’s SIM7070G](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#SIMCom-SIM7070G)
-   [Thales’ PLS62-W](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#Thales-PLS62-W)
-   [Telit’s LE910C1](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#Telit-LE910C1)

In this guide, we use our Traffic Monitor insight tool to analyze traffic. You can alternatively use [Wireshark](https://www.wireshark.org/), for example.

If you are in the process of testing connectivity, it’s highly recommended that you read through this article first before testing connectivity by sending data: [How do I test connectivity with AT commands?](http://link/)

**Warning**: This article requires knowledge of connecting to your device’s cellular module/modem.

## Table of Contents

-   [The overall structure and standard result](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#the-overall-structure-and-standard-result)
-   [Quectel’s BG96](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#quectels-bg-96)
-   [SIMCom’s SIM7070G](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#sim-coms-sim-7070-g)
-   [Thales’ PLS62-W](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#thales-pls-62-w)
-   [Telit’s LE910C1](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#telits-le-910-c-1)
    -   [Build your own network](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#build-yourown-network)
    -   [Start testing IoT SIMs for free](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#start-testing-io-t-si-ms-for-free)
    -   [Related content](https://onomondo.com/blog/how-to-ping-your-modem-using-at-commands/#related-content)

## The overall structure and standard result

The overall structure for pinging 8.8.8.8 with all modems is:

1.  Set the APN
2.  Attach to the network
3.  Establish a data connection
4.  Ping 8.8.8.8 

You will most likely be able to use the same approach for each of the manufacturers’ other modems, but there might be slight variations, so please consult the documentation, your supplier, or the manufacturer themselves if the sequences below do not work.

No matter which modem you are using, the result should be the following in the Traffic Monitor:

![8.8.8.8 modem ping result](https://cdn.growth.onomondo.com/app/uploads/2023/04/25134620/8.8.8.8-modem-ping-result.png)

## Quectel’s BG96

1\. Set the APN

Issue **AT+CGDCONT=1,”IP”,”operator”**, this defines the PDP context.

```
AT+CGDCONT=1,"IP","onomondo"
OK
```

2.Attach to the network

Check the connection with **AT+COPS?**.

```
AT+COPS?
+COPS: 0,0,”T-Mobile USA”,7
OK
```

The modem might already have connected when it was booted up, if this isn’t the case, then do the following:

1.  Issue **AT+COPS=0**, this will make the modem automatically choose a network.
2.  Wait for **AT+CEREG?** or **AT+CGREG?** to return x,5, where x doesn’t matter.

3\. Establish a data connection

Issue AT+QIACT=1,1 which activates the PDP context

4\. Ping 8.8.8.8

Issue **AT+QPING=1,”8.8.8.8″,5,5,**

This pings 8.8.8.8 five times with a timeout of five seconds.

## SIMCom’s SIM7070G

1\. Set the APN

Issue **AT+CGDCONT=1,”IP”,”operator”**, this defines the PDP context.

```
AT+CGDCONT=1,"IP","onomondo"
OK
```

2\. Attach to the network

Check the connection with **AT+COPS?**.

```
AT+COPS?
+COPS: 0,0,”T-Mobile USA”,7
OK
```

The modem might already have connected when it was booted up, if this isn’t the case, then do the following:

1.  Issue **AT+COPS=0**, this will make the modem automatically choose a network
2.  Wait for **AT+CEREG?** or **AT+CGREG?** to return x,5, where x doesn’t matter

3\. Establish a data connection

Issue AT+CNACT=0,1 which activates the PDP context

4\. Ping 8.8.8.8

Select PDP context ID for pinging **AT+SNPDPID=0**

Issue **AT+SNPING4=”8.8.8.8″,5,16,5000,** this pings 8.8.8.8 five times with a timeout of five seconds.

## Thales’ PLS62-W

1\. Set the APN

Issue **AT+CGDCONT=1,”IP”,”operator”**, this defines the PDP context.

```
AT+CGDCONT=1,"IP","onomondo"
OK
```

2\. Attach to the network

Check the connection with **AT+COPS?**.

```
AT+COPS?
+COPS: 0,0,”T-Mobile USA”,7
OK
```

The modem might already have connected when it was booted up, if this isn’t the case, then do the following:

1.  Issue **AT+COPS=0**, this will make the modem automatically choose a network
2.  Wait for **AT+CEREG?** or **AT+CGREG?** to return x,5, where x doesn’t matter

3\. Establish a data connection

Issue **AT^SICA=1,1** which activates the PDP context

Get the IP of your device with **AT+CGPADDR=1,** this is not optional

4\. Ping 8.8.8.8

Issue **AT^SISX=Ping,1,”8.8.8.8″,5,5000,** this pings 8.8.8.8 five times with a timeout of five seconds.

## Telit’s LE910C1

1\. Set the APN

Issue **AT+CGDCONT=1,”IP”,”operator”**, this defines the PDP context.

```
AT+CGDCONT=1,"IP","onomondo"
OK
```

2\. Attach to the network

Check the connection with **AT+COPS?**.

```
AT+COPS?
+COPS: 0,0,”T-Mobile USA”,7
OK
```

The modem might already have connected when it was booted up, if this isn’t the case, then do the following:

1.  Issue **AT+COPS=0**, this will make the modem automatically choose a network
2.  Wait for **AT+CEREG?** or **AT+CGREG?** to return x,5, where x doesn’t matter

3\. Establish a data connection

Issue **AT#SGACT=1,1** which activates the PDP context

4\. Ping 8.8.8.8

Issue **AT#PING=”8.8.8.8″,5,** this pings 8.8.8.8 five times with a timeout of five seconds
