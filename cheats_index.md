# Cheats

## Main Cheats

  * [[vimcheat|Vim]]
  * [[shellcheat|Bash/Shell]]

## [[syscheat|Sysadmin Cheats]]

## [[toolscheat|Tool Cheats]]

## [[General_Programming|Programming Language Cheats]]

## Developer Cheats

  * [[makecheats|Make & CMake]]
  * [[gitcheats|Git]]
  * [[djangocheat|Django]]
  * [[dbscheat|Databases]]
  * [[pragmaticcheat|Pragmatic Programmers]]
  * [[RESTcheat|RESTful API]]
  * [[bootstrapcheat|bootstrapcheat]]
