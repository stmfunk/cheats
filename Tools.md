# Tools

## Electrical Engineering Tools
  * *Saleae*
    * [Socket API (Logic v1.x Only)](https://support.saleae.com/saleae-api-and-sdk/socket-api)
  * *Lindstrom*
    * [High-End Wire Cutters](https://www.lindstromtools.com/ie_en/ultra-flushr-precision-cut-diagonal-cutter-with-oval-head---esd-safe-handle-0-1-1-mm-8142.html)
    * [Set of Pliers](https://www.cooksongold.com/Jewellery-Tools/Lindstrom-Set-Of-4,-Assorted--------Supreme-Pliers-And-80-Series-Cutter-prcode-997-LS01?p=gs&gad_source=1&gclid=Cj0KCQiAgqGrBhDtARIsAM5s0_kjBfopBw6FPtkDHaqfcJl3x_3L_ouc1WZy6BERGp6aJB-SchgPjMMaAomEEALw_wcB&gclsrc=aw.ds)
  * *Knipex*
    * [More Reasonable Snips](https://www.amazon.co.uk/Knipex-78-06-125-Electronic/dp/B07QCGBWPH/ref=asc_df_B07QCGBWPH/?tag=&linkCode=df0&hvadid=372384296836&hvpos=&hvnetw=g&hvrand=5141539544859664333&hvpone=&hvptwo=&hvqmt=&hvdev=c&hvdvcmdl=&hvlocint=&hvlocphy=1007850&hvtargid=pla-811726114525&mcid=fdaf3c6be5683f50960f6371cc3d126c&th=1&ref=&adgrpid=79390523034)

## Woodworking Tools
