# MIPS Cheat
## Operating System Calls
$v0         :       Register where we load the code for the syscall (what kind of operation)

Call Codes  :       8   Read into address specified in a0, the number of characters specified in a1
                    4   Print string referenced in address stored at a0
                    10  Exit program

## Sections
.data       :       Specify data storage region.
.text       :       Specify the executable region.
.asciiz     :       Specify a region of ascii text.

## Instructions
j t         :       Jump to the target address

## Labels
foo:        :       Create a label foo that can be referenced by the assembler.
main:       :       The start point for execution, predefined label.
