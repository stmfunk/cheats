Basics
======
REST                :       Representational State Transfer
SOAP                :       Simple Object Access Protocol (Older alternative to REST). 
RPC                 :       Remote Procedure call. A procedure is executed is executed in another 
                                address space (typically on a networked computer), but called locally as
                                if it were any other procedure call. See also Remote Method Invocation (RMI)

HTTP
====
Verbs
-----
GET                 :       Fetch a resource from the server.
POST                :       Create a resource on the server. Used to when specific information
                                about resource may not be known.
PUT                 :       Create or update a resource, but much more specific than POST, when 
                                exact details may be known
DELETE              :       Remove a resource from the server.

Response Codes
-------- -----
10x                 :       Informational Responses

20x                 :       Success
200                 :       OK, the request completed successfully.
201                 :       Request Succes

30x                 :       Redirect Messages

40x                 :       Client Error Responses
400                 :       Bad Request, invalid syntax.
404                 :       Page not found
405                 :       Access not allowed.
409                 :       Conflict.
408                 :       Request timeout.

50x                 :       Server error
500                 :       Internal server error
502                 :       Bad gateway (server received an error response while acting as a gateway)
