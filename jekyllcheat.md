# Jekyll
jekyll new                  :       Create a new jekyll site

bundle exec jekyll serve    :       Run jekyll test site for the first time
jekyll serve                :       Run the jekyll site after.

## Files
config.yaml                 :       Settings file
index.html                  :       File stores the entire site.
Gemfile                     :       Stores dependencies
index.md                    :       Default page for the front.


## Commands

{% comment %}               :       Begin commented section.
{% endcomment %}            :       End comment section
