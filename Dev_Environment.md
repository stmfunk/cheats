# Dev Environment Index 

  * [[NeoVim]]
  * [[shellcheat|Bash]]

# Embedded Tools

  * [[minicomcheat|Minicom]]

## Other References 

  * [Emacs Evil Mode](https://blog.jeaye.com/2015/10/24/emacs-vim/)
