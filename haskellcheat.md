# Haskell
## IO
getLine                             :       Take value given at stdin and assign it to function. 
                                               val <- getLine

## Syntax
``                                  :       Make an operation infix
!!                                  :       Index a list item "123" !! 1 = 2

## Precedence
Function calls have the highest precendence binding to the nearest argument.

## Conditionals
ifthenelse                          :      An expression that returns either if or else depending 
                                            on the condition

## Builtins
succ n                              :       Return the integer after n
init                                :       Returns entire list minus the last element
last                                :       Returns last element
head                                :       First element
tail                                :       All but last element
take n xs                           :       Returns the first n of a list
drop n xs                           :       Returns the list minus first n

show                                :       Represent as a string.
read                                :       Convert string into a type represented as a member of Read
compare                             :       Return an ordering of the two inputs (GT, EQ, LT).

## Typeclasses
Typeclasses are sets of types that are used to define the set of types a function works over.

## Commands
:t                                  :       Display the type of an object.


## Currying
All functions in haskell take a single parameter. A multi-parameter program is simply one that 
takes a single parameter which creates a function that takes an additional parameter.
