Basics
======
No variable declaration in ruby

Fully object orientated everything is an object.
Imperative.
Strongly typed.
Dynamically typed
Doesn't support multiple inheritence 
Classes are also modules.
A class is an intance of the Class class.
Class is a subclass of Module, which is a subclass of Object
Object is the common ancestor of everything.

Uses duck-typing (it will perform a method on any type that supports it)

.methods                :       Gives any objects methods.
.class                  :       Gives an objects class.
.object_id              :       Unique id that differentiates objects.
.superclass             :       Give objects superclass. Stacks



Equality                :       Different object have different definitions of equality
There are booleans but all objects have a true and false value.

nil,false are the only false objects

and,&&,or,||            :       Shorthand conditionals
&,|                     :       Conditionals that force execution of entire statement

a <=> b                 :       Spaceship operator -1 if b is greater 1 if a is greater 0 if equal.

__END__                 :       Can be used on a line of its own to determine the end of a program.
                                    Data can be placed after it which can be used by the script.

Ruby uses newlines as statement terminators but only if the newline forms a logical end to statement
otherwise it will read the next line.

; can also be used to terminate a statement.

Data Types
==== =====
Variables that begin with a capital letter are constants. 
$var                    :       Variables beginning with a dollar sign are global.
@var                    :       Instance variable
@@var                   :       Class variable.


Numbers
-------
Numeric                 :       Base class of all number types.
Integer                 :       Super type of all integers. The subtypes are
                                    Fixnum:  integers that fit in 31 bits
                                    Bignum:  larger integers
                                Conversion between these two is automatic meaning no overflow ever.

Float                   :       Uses the built in float type of the machine.
Complex                 :       Represent complex numbers
BigDecimal              :       Arbitrary precision decimal, uses decimal representation instead of binary like a float
Rational                :       You see the pattern.

All numeric objects are immutable.
Underscores embedded in numbers are ignored and can be used for styling. 1_000

0x/0X                   :       To enter a hex number
0b/0B                   :       Binary
0                       :       Octal

e/E                     :       In a float for an exponent.

Ruby uses normal arithmetic and operators but also some methods that behave differently.
div                     :       For integer division
fdiv                    :       For floating point division.
quo                     :       Returns rational number
divmod                  :       Returns mod and div


Strings
-------
''                      :       A stricter quoting. Doesn't support many escape characters. Can be split
                                    across multiple lines using \ and '\
""                      :       More flexible quoting, allows escape characters and unicode insertion. 
                                    Also support string interpolation. Can span multiple lines newlines included
                                    unless escaped. Adjacent strings are concatenated unless on multiple lines, then they
                                    must be escaped.
                                    #{expr} : Will be evaluated by ruby interpreter.
"""                     :       Open a triple quote.


printf
sprintf                 :       Both supported in ruby with traditional syntax.
  

Tools
-----
ri                      :       View documentation for ruby stuff.

Classes
=======
initialize              :       Like init in python used to initialize object.
@x                      :       To define an instance variable.
@@x                     :       To define a class variable

Convention is to use underscore style to write instance variable.
Constants are written ALL_CAPS
Test functions conventionally end with a question mark


attr                    :       Keywords beginning with attr define instances variables. 
                                    attr on its own define an instance variable and a method of the same name to access it.

attr_accessor           :       Define a variable getter and setter

Open Classes
---- -------
Open classes allow you to modify an existing class after it has been created. 
We can modify an open class simply by redefining a class after it has been previously defined. 

Modules
=======
Modules are used to define behaviours for classes similiar to interfaces only the may include implementations.

include Md              :       Used in a class to include a mixin.

enumberable             :       Must implement each
comparable              :       Must implement <=>


IO
==
puts to print a string

Data Structures
---- ----------
Ranges                  :       Stored in a range object
                                    .to_a convert range object to list
                                    .min, .max return max and min in range
                                    .reject(|x| x > 2) filter out elements greater than 2
                                    .each do |x| block perform action on every element
(n..m)                  :       Create range from n to m inclusive
(n...m)                 :       Create range from n to m exclusive

Array                   :       Main method of storing data in ruby.
                                    Support reverse indexing.
                                    Similiar to lists in Python
                                    .pop,.push

Hashes                  :       Like dictionarys in python


:s                      :       Define a symbol. Symbols are labels. All symbols that have the 
                                    same identifier are the same object.



Strings
=======
""                      :       String allows evaluation, interpolation etc.
''                      :       Strict string no evaluation.
.sub                    :       Replace first occurences in object with parameter
.gsub                   :       Replace all occurences in object with parameter
.gsub!,sub!             :       Modify the string they are called on itself.


Conditionals
============
unless                  :       Essentially a negated if statement

stmt if cond            :       Inline conditionals put the statement first then the conditional.

if
  stmts
end                     :       Execute statements in a block.

not,!                   :       Both mean not


while, until            :       Create while loops and negated while loops.

Regex
-----
/pattern/               :       Represents a regex pattern
/pattern/i+             :       You can specify pattern options after the pattern.
%r!pattern!             :       To choose your delimiter
=~                      :       Regex equals, match an expression against a regular expression

options                 :       i, ignore case
                                o, perform interpolations only once
                                x, ignore whitespace allow comments
                                m, match multiple lines recongise newline as character.

Functions
---------
method?                 :       Convention that methods ending in ? return a boolean.
method!                 :       Convention that these methods should be used cautiously 
method=                 :       Can be invoked by assignment syntax to the method.

All functions have a return value;

f(x)+1 is not the same as f (x)+1 the space between the function and the brackets turn it from an
argument list to a part of an expression.


Code block              :       Like anonymous functions. Specify with { } or do end use |x| to specify parameters.
                                    yield keyword invokes a code block passed in. Can take parameters also.
                                    .call to call
                                    &block to pass it as a parameter


o.x=()                  :       When a method definition ends with = it can be invoked by assigning to it.
