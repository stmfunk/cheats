# WhereverSIM

   * [WhereverSIM SIM800L Docs](https://help.whereversim.de/wie-kann-ich-meine-data-karte-per-at-kommandos-auf-dem-sim800l-verbinden)
   * [WhereverSIM Raspberry Pi Docs](https://help.whereversim.de/wie-richte-ich-das-sim7600-modem-zusammen-mit-einem-raspberrypi-ein)

## How can I connect my data+ card to the SIM800L using AT commands?

The SIM800L is configured for use with your data+ SIM card using AT commands via serial interface (RX & TX PINs). Please ensure that the module is connected to your Arduino, Raspberry or similar device according to specifications. A voltage converter is often necessary to achieve the required voltage and the power provided via the I/O pins may not be sufficient, which leads to frequent errors.

In the Arduino IDE there are alternative function packages for using the SIM800L, which make configuration using sketches easier. 

The following AT commands can be used in this order to connect the device to the Internet:

```
AT<br><em>OK</em>
```

Checks whether the serial connection is working correctly.

```
AT+CFUN=1<br><em>OK</em>
```

Activates all functionalities of the modem.

```
AT+CPIN?<br><em>+CPIN: READY</em>
```

Shows with the READY response that the SIM card is working and ready.

```
AT+CGATT=1<br><em>OK</em>
```

Activates the GPRS connection.

```
AT+CSTT="wsim","",""<br>OK
```

Sets the correct APN without username and password.

```
AT+CIICR<br>OK
```

Builds the PDP context, i.e. the Internet session.

```
AT+CIFSR<br><em>100.XXX.XXX.XXX</em>
```

Outputs the assigned IP address.

```
AT+CIPSTART="TCP","exploreembedded.com",80<br><em>OK</em><br><em>CONNECT OK</em>
```

This establishes a TCP connection.

```
AT+CIPSEND=63<br><em>&gt;</em>
```

Indicates that there are 63 characters in which the TCP instruction is transmitted.

```
GET exploreembedded.com/wiki/images/1/15/Hello.txt HTTP/1.0
```

An example query that retrieves the contents of a text file.

Since AT commands can differ depending on the module used, these instructions can only be used to a limited extent for modules other than the SIM800L. If in doubt, please check your manufacturer's documentation for the corresponding AT commands.
