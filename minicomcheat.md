# Minicom

## Features
   * [Runscript (minicom scripting language), Man Pages](https://linux.die.net/man/1/runscript)
   * [Minicom, Man Pages](https://linux.die.net/man/1/minicom)

## Tutorials

   * [Minicom Tutorial](https://derrekito.github.io/Minicom-tutorial/)
   * [Random Instructional](http://netwinder.osuosl.org/users/u/urnaik/public_html/ixp1200_howto.html#UsingMinicom)
   * [Minicom & Seyon Tutorial](https://www.oreilly.com/openbook/debian/book/ch11_07.html)
   - [ ]  [Runscript Demo (Download this)](https://github.com/Distrotech/minicom/blob/master/extras/scriptdemo)
   - [ ]  [Runscript Demo 2 (Download this)](https://github.com/Distrotech/minicom/blob/master/extras/unixlogin)


