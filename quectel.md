# Quectel

   * [[bg_95_article_mongoto|BG95 Article (Mongoto)]]
   * ATD to dial a connection. If response is CONNECT it's connected. Check gprs.c for example.
   * [[wherever_sim|WhereverSIM support]]
   * [[connect_bg95to_network|Connect BG95 to network (EsEye)]]
   * [[bg95_qping_guide|Ping a server using AT Commands (Onomondo)]]
   * [Great list of basic AT command stuff](https://www.emnify.com/developer-blog/at-commands-for-cellular-modules)


## Factoids

   * AT+CLAC will print out all supported commands.


## Issues Uncovered

   * FPLMN List being full. <https://onomondo.com/blog/how-to-clear-the-fplmn-list-on-a-sim/>
      Run command: `AT+CRSM=176,28539,0,0,12`. If result isn't a string of 0xFFs then the list needs to be cleared.
   * Response `AT+QPING: 561`. This was occuring when the APN was incorrect.
   * APN settings for Vodafone Ireland are "live.vodafone.com" *NOT* isp.vodafone.ie.
   * [WhereverSIM](https://help.whereversim.de/) APN appears to be "wsim", but Ger says it's "LM". 
   * Network registration Denied `CREG: 1,3`

  
