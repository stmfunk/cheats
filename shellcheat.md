# COMMON
## Options
--                                  :       End of command options. Many commands use this to signify that no more
                                                command options will follow and everything after will be a positional
                                                argument. e.g. `grep -- -v file` will search for -v in file.
## Shortcuts
Ctrl-a                              :       Start of command
Ctrl-e                              :       Go to end of command
Ctrl-k                              :       Delete from cursor to end of line
Ctrl-u                              :       Delete from cursor to beginning of line

Ctrl-w                              :       Delete backwords one word.
Ctrl-h                              :       Delete backwords one character.


Alt-b                               :       Move backwards one word.
Alt-f                               :       Move forwards one word.


Ctrl-z                              :       Suspend the currently running process 
                                                and return control to bash.


## Builtins
jobs                                :       List current shell jobs.
fg                                  :       Return background job to control.
trap                                :       Execute a command when a signal is received.
                                                trap 'cmd' SIG



## Quotes
meta-characters                     :       Meta characters are characters with a special meaning
quoting                             :       Allows the use of meta characters as IO 

\                                   :       Strongest form of quotation. 
                                                Escapes the character after it.
'                                   :       Severe quoting everything inside will be viewed as base character no pattern matching.
"                                   


## Meta-Character List
$#                                  :       Number of arguments passed to script.
$?                                  :       Status of last command.               
$-                                  :       Flags past to shell
$@                                  :       Original arguments to shell.
\$$                                  :       Pid of the script.
$!                                  :       PID of last background job.
!!                                  :       Execute the previous command again


# ZSH
compsys                             :       Tabcompletion system. 
compinstall                         :       Compsys configuration tool.

arrays                              :       Arrays in zsh are made using ( ) instead of quotes. 
                                            They are indexable and vector data.
                                                e.g. foo=(This is an array)
                                                     foo[4] = array (note indexing from 1)

## Shell Options - setopt OPTION
LOCAL_OPTIONS                       :       You can set this at the start of a function to ensure any options set in the function
                                                are reverted after it completes.
LOCAL_TRAPS                         :       Resets any traps set in function once it completes.
XTRACE                              :       Print every line as it is executed in a shell script.

MULTIOS                             :       This option allows for multiple IO redirects. This affects the piping.
                                                Place brackets around command to manipulate this. 

## Parameters
$                                   :       Used in parameter expansion. 
                                                Place before a variable name to replace with value stored in variable.
${}                                 :       Curly braces signify a block of expansion 


## Functions
external functions                  :       Functions can be defined in their own files. They need to be kept in a file
                                                with the same name as the function. You can write the function body directly
                                                into the script or define named functions in it and call it at the end. Of the
                                                file. 
autoload fn                         :       Load function automatically when you call it's name no sooner. 
$fpath                              :       Lists directories to search for functions for autoloading. 


## Links
 * [Advanced Bash-Scripting Guide](https://tldp.org/LDP/abs/html/)
 * [Bash shortcuts](http://www.skorks.com/2009/09/bash-shortcuts-for-maximum-productivity/)
