Preface
======
What makes a Pragmatic Programmer?
---- ----- - --------- -----------

 * Early adopter/fast adopter
 * Inquisitive
 * Critical thinker
 * Realistic
 * Jack of all trades

#?1# Care about your craft

#?2# Think! About your work

Kaizen          :           The concept of continual small improvements.


>> 1
A Pragmatic Philosophy
= ========= ==========
>> 1.1
The Cat ate my Source Code
--- --- --- -- ------ ----
"The greatest of all weaknesses is the fear of appearing weak"

Take responsibility for yourself and your actions. Don't be afraid to admit ignorance or error.
It is up to you to provide solutions not excuses.

#?3# Provide options don't make lame excuses.

Make sure your excuses sound reasonable before you tell your boss.


>>1.2
Software Entropy
-------- -------
"software rot" is the increase in the entropy or the amount of disorder in software.

#?4# Don't live with Broken Windows

This means don't leave bad design, wrong decisions or poor code unrepaired. If you don't have time
to fix it flag it or comment it out til later. Neglect accelerates rot faster than any other factor.


>>1.3
Stone Soup and Boiled Frogs
----- ---- --- ------ -----
When starting a project or making a change systems suffer from "start-up" fatigue. People will delay
the process, demand budgets, guard their own resources. In these situations figure out what you
can ask for and receive, and use the results to coax out more enthusiasm and resources.

"It's easier to ask for forgiveness than permission."

#?5# Be a Catalyst for Change

It is also important to remember not to focus too tightly on something. Try to keep the whole situation in
mind.

#?6# Remember the Big Picture

>>1.4
Good-Enough Software
----------- --------
"Striving to better, oft we mar what's well"

It is important to understand that your software can only be so good. This means learning to accept what you
reasonably achieve and not push deadlines and delay software in the interest of making it a little better.

#?7# Make Quality a Requirements Issue

Great software today is preferable to perfect software tomorrow.

>>1.5
Your Knowledge Portfolio
---- --------- ---------
"An investment in knowledge always pays the best interest"

Developer knowledge our most valuable asset. It is also an expiring asset. We will refer to all the facts a programmer
knows as their knowledge portfolio. It needs to be managed just like a stock portfolio:

1. Serious investors invest regularly - as a habit.
2. Diversification is the key to long term success. (the more different things you know the more valuable you are.)
3. Smart investors balance their portfolios between conservative and high-risk, high-reward investments.
4. Investors try to buy low and sell high for maximum return.
5. Portfolios should be reviewed and rebalanced periodically.


#?8# Invest regularly in you knowledge portfolio

Goals:
 * Learn at least one new language every year. 
 * Read a technical book each quarter. 
 * Read nontechnical books too. 
 * Take classes. 
 * Participate in local user groups.
 * Experiment with different environments. (experiment with IDEs etc.)
 * Stay current. (subscribe to trade magazines and journals)
 * Get wired

When you have a question you don't know the answer to FIND OUT!

Critically analyse what you read and hear. Think about the motivation behind the promotion of something, go one 
level deeper. Take nothing at face value. 

#?9# Critically Analyze what you Read and Hear


>>1.6
Communicate!
------------
"I believe that it is better to be looked over than it is to be overlooked"

Plan what you want to say, Write an outline. Ask yourself does this get across whatever I'm trying to say?

Know your audience. Form a mental picture of your audience. Present it in a manner that suits the group you
are communicating with.

Choose your moment. Understand what your audience's priority is when you are talking to them. If something is 
likely to be strong in their mind use the opportunity to get something important across. Ask is it an appropriate
time.

Wisdom Acrostic
                    W hat do you want them to learn?
      What is their i nterest in what you've got to say?
                How s ophisticated are they?
           How much d etail do they want?
Whom do you want to o wn the information?
        How can you m otivate them to listen to you?

Adjust the style of your delivery to suit your audience.

Make your delivery vehicle look good. It will draw attention and reflect what you are saying in a more positive light.
Use Latex or word and spend some time tuning it. 

Involve your audience in the creation of the presentation. 

Be a listener. Encourage people to talk by asking questions, or have them summarize what you are telling them. 

Get back to people. Always respond to emails even if it's a simple I'll get back to you.

#?10# It's both what you say and the way you say it. 

>>2
A Pragmatic Approach
= ========= ========
>>2.7
The Evils of Duplication 
--- ----- -- ----------- 
Maintenance is a constant state for a programmer. Things constantly need updating, fixing, changing. If we 
have duplicate knowledge it makes maintenance a nightmare. Follow the DRY principle

"Every piece of knowledge must have a single, unambiguous, authoritative representation within the system"

#?11# DRY - Don't Repeat Yourself

How does duplication arise:
 * Imposed duplication: have no choice the environment requires it. 
 * Inadvertent duplication
 * Impatient duplication: getting lazy
 * Inter-developer duplication

Imposed duplication. Always look for techniques that allow us to generate data on the fly rather than duplicating
even if we are using multiple languages or have interacting code. Remember when writing comments not to explain
small details. Use them to give an impression of what is going on at a deeper level. 

Inadvertent Duplication. Sometimes duplication is down to pure design. Attempt to analyse a problem in such a way
that information does not need to updated in multiple objects. Where a value can be calculated from existing
other information, calculate don't store. If performance is an issue use intelligent caching. Always use an accessor 
function to read and write attributes. 

Impatient Duplication. "Short cuts make for long delays". Don't just copy code around to make life easier. 

Inter-developer Duplication. Overcome this with a strong technical leader and a well understood division of 
responsibilities. The best way however is frequent and active communication. Appoint a project librarian to
facilitate the exchange of knowledge. Have a central place in the source tree where utility routines are implemented
and read other people's source code and documentation. 

#?12# Make It Easy to Reuse

Foster an environment where it's easier to find and reuse existing stuff than to write it yourself.  


>>2.8
Orthogonality
-------------
"Orthogonality" is a term borrowed from geometry. When two lines meet at right angles to each other they are
independent. Move along one line and your position projected onto the other doesn't change. In computing two or more
things are orthogonal if changes to one does not affect any of the others. 

#?13# Eliminate Effects Between Unrelated Things

Design components that are self contained with a single well defined purpose (cohesive). This will result in increased
productivity and reduced risk.

Productivity:
 * Changes are localised. A component can be built tested and forgotten. 
 * Reuse is promoted. A component with a specific well defined responsibility can be combined with
    new components in ways not originally envisioned. More loosely coupled easier to re-engineer.
 * If one component does M things and one does N things, if they are orthogonal, then the result should
    do M x N things. If they aren't then there will be overlap. 

Risk
 * Diseased sections of code are isolated. If a module is sick it is less likely to spread symptoms 
    around the rest of the system. 
 * Resulting system is less fragile. Changes are restricted to one area. 
 * Better tested, easier to design and run tests on its components.
 * Less tightly tied to a vendor product or platform because interface will be isolated to smaller parts.

Project Teams. Teams organised with lots of overlap tend to be confused about responsibilities. Every change
needs a meeting of the entire team. The more people need to be involved in discussing a change the less 
orthogonal the team. 

Design. Systems should be composed of a set of cooperating modules each of which implements functionality
independent of the others. Sometimes this means organising in layers. Ask yourself: If I dramatically change
the requirements behind a particular function how many modules are affected? 

"Don't rely on the properties of things you can't control"

Toolkits and Libraries. When you bring in a toolkit or a library, ask yourself whether it imposes changes in
your code that shouldn't be there. Aspect Orientated Programming lets you express in one place, behaviour 
that would otherwise be distributed throughout your source-code.

Coding. Keep your code decoupled, never use a module's implementation in your code. Don't reveal anything about
your module that needn't be known. When changing an objects state, let that object change itself. Only use global 
data as a last resort. Any access, read or write, binds separate objects together more closely. Avoid similar 
functions. Functions that share allot of code is a symptom of structural problems.

Testing. If our system is orthogonal it is much easier to test. We should be able to test on a module level.
Testing and bug fixing will give you a good sense of orthogonality. How much code do you have to pull in to
test a module, or how many modules need to be changed to fix a bugs.

>>2.9
Reversibility
-------------
It is important to acknowledge that some critical decisions can be almost irreversible in the later stages of 
a project without tremendous cost. Keeping this in mind it is always a good idea to make these decisions as 
abstract as possible. Instead of weaving proprietary APIs through your code create a container object.

#?14# There are No Final Decisions

>>2.10
Tracer Bullets
------ -------
This concept is similar to the walking skeleton approach discussed by the Amazon speaker. The idea is to reduce the 
spent specifying how everything works and more time experimenting. We create a working bare bones model fast and then
we know where we stand. We can then add features "fatten the lamb" so to speak or if we are way off base we can scrap
it and build a replacement equally rapidly. A core idea is also not to build garbo code but to make a simple version of
the final product early. 

#?15# Use Tracer Bullets to find the Target

With Tracer Code:

Users get something working early. Users rather a simple product in front of them to a complex one in production. Being
able to demonstrate any progress is encouraging. 

Developers build a structure to work in. It gives engineers a starting point and takes the intimidation out of the task. 

You have an integration platform. It supports connecting components continuously, instead of a long complex integration
phase that can cause more problems. 

You have something to demo. 

You feel better for progress. You tackle and complete use cases one at a time and have a more visual progress. 


>>2.11
Prototypes and Post-it Notes
---------- --- ------- -----
Different Prototypes are designed to answer different questions. We might make a UI prototype that helps us design 
the interface. A database prototype helps us design our schema. Prototypes can ignore details of final implementations.
Remember you if you are including too many details then you are at risk of writing bad code rather than a prototype.
You can prototype unproven, experimental or things you aren't familiar with. 

#?16# Prototype to learn

When prototyping you can ignore:
 * Correctness (include fake data)
 * Completeness (limited functionality)
 * Robustness
 * Style (leave out comments or docs)

Sometimes we will prototype the entire system. This will help use get a feel for how the system hangs together. We can
work out bugs or bad design before we ever start implementing. We can even prototype with diagrams.

!! Warning: When writing a prototype make sure that everybody knows you are writing disposable code. Flag it, put in big
scary comments and highlight it in error messages. Sometimes management pressure will push you to deploy a prototype. If 
you fear this happening DON'T BUILD A PROTOTYPE!


>>2.12
Domain Languages
------ ---------
When we try to solve a problem in a specific language, the language we use affects how we think about the problem. It may
make the best solution less obvious or difficult to implement, it may make suboptimal solutions seem simpler. In some 
writing a small context specific language can make a solution easier and more flexible. 

#?17# Program close to the domain problem

Domain languages allow you to encode the problem itself instead of working the problem into code. It makes it easier to 
give the user useful feedback and simpler for the user to use themselves.

When defining a language try, to use BNF to make parsing simpler. Languages can vary in sophistication, complexity and
readability. Some languages simply encode data, others are programmable instruction sets that are executed by an interpreter.

When design a DSL it is always a trade off between ease of maintenance and ease of development. Sometimes it pays to go the
extra mile.


>>2.13
Estimating
----------

#?18# Estimate to avoid surprises

When making an estimate the first consideration is how accurate do you want to be. You must consider the units you want to 
give your answer which can change how your estimate is received. For example 26 weeks will be considered a more accurate
estimate than half a year.

We arrive at estimates by created models of the problem. This can be as simple as running through the process in your head
or making a spreadsheet calculations or anything in between. When making an estimate the first thing is to understand what
you are being asked. This means knowing what constraints to consider and leave out how much detail to go into. The you can 
model the system however suites the answer you need best. 

It is a good idea to break down the model into that affect the end result. Some are more important than others. Then input 
your known or best guess value parameters. Then you will have an answer and it is often better to have a reasonable range 
than a single number. It is also a good idea to keep track of your estimates, and review them after you know the answers. 
This will help improve your skills going forward and identify where you went wrong.

With project schedules it is sometimes a better idea to start working and figure out the schedule in more and more detail the
more you know. The schedule gets updated every time you assess the project and becomes more precise and understood each time.

#?19# Iterate the Schedule with the Code


>>3
The Basic Tools
=== ===== =====
>>3.1
The Power of Plain Text
--- ----- -- ----- ----

#?20# Keep Knowledge in Plain Text

Plain text is slower and larger way to store data than binary. But it can describe itself and be easily understood.
Plain text is also easier to parse without fully understanding the entire implementation. Key data can be extracted easily
and reformatted.

URLS
====
[www.pragmaticprogrammer.com] for source code.

    vim: set spell:
