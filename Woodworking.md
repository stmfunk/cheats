= WoodWorking =

  * [[Tools#Woodworking Tools|Woodworking Tools]] 


== Tools ==

  * Clamps
    * [[https://www.sautershop.com/clamps/body-clamps/sauter-body-clamps/?gclid=CjwKCAjwrfCRBhAXEiwAnkmKmdgxDHBAGD7RFogrmdu7FGz29wCwsEGfsf6MuD4aBS8RgppDwy3GERoCaJAQAvD_BwE|Sauter Great Clamps]]

== Design Aids ==

  * [[https://www.sketchup.com/plans-and-pricing#for-personal|Sketchup]]

== Tutorials ==

  * [[https://theweekendwoodworker.com/wp-content/uploads/TWW_Tool_Guide_.pdf|Weekend Woodworker Tool List]]

== Suppliers ==

  * [[https://www.thecarpentrystore.com|Carpentry Store Dublin]]
