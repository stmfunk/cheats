# NeoVim

## Configuration

  * [Switch from init.vim to init.lua](https://www.notonlycode.org/neovim-lua-config/)

## Plugins

  * *How Tos*
    *  [Linode How to install Plugins](https://www.linode.com/docs/guides/how-to-install-neovim-and-plugins-with-vim-plug/)
  * *Code Completion*
    *  LPS
      *  [NeoVim Native LSP Tutorial](https://www.youtube.com/watch?v=NXysez2vS4Q)
    *  [nvim-compe](https://github.com/hrsh7th/nvim-compe)
    *  [nvim-cmp](https://github.com/hrsh7th/nvim-cmp)
    *  [completion.nvim](https://github.com/nvim-lua/completion-nvim) *Unmaintained*
    *  [NCM2](https://github.com/ncm2/ncm2)
  * *Treesitter*
    *  [Treesitter Introduction](https://tree-sitter.github.io/tree-sitter/)
    *  [Treesitter Repo, Adding Parsers](https://github.com/nvim-treesitter/nvim-treesitter#adding-parsers)
  * *OrgMode*
    *  [NeoVim OrgMode Repo](https://github.com/nvim-orgmode/orgmode)
    *  [Neorg Repo](https://github.com/nvim-neorg/neorg)
    *  [Vim Org Mode Tutorial](https://www.youtube.com/watch?v=lQ0wOGEB9co)
    *  [Vimwiki to OrgMode](https://firminmartin.com/en/posts/2020/09/migrate_from_vimwiki_to_org-mode/)
    *  [OrgMode Features](https://orgmode.org/features.html)
    *  [Vim/Neovim Managing Notes and Todo Lists](https://alpha2phi.medium.com/vim-neovim-managing-notes-and-todo-list-8ae8e3db6464)
  *  [Tabnine](https://alpha2phi.medium.com/neovim-and-vs-code-ai-assisted-code-completion-4991e993e645)
  *  [VimWiki Installation](https://github.com/vimwiki/vimwiki#installation)
  *  [Vim Table Mode](https://github.com/dhruvasagar/vim-table-mode)
  *  [Useful Neovim Plugins](https://alpha2phi.medium.com/new-neovim-plugins-to-improve-development-workflow-33419d74e9ac)
  *  
    *  [Vimux](https://github.com/preservim/vimux)
    *  [Vim & Tmux](https://www.bugsnag.com/blog/tmux-and-vim)
    *  [Configuring Vim & Tmux](https://andela.com/insights/configuring-vim-and-tmux-to-boost-your-productivity/)
  * *Tagging & Code Completion*
    *  [CScope](http://cscope.sourceforge.net)
    *  [Vim/CScope Tutorial](http://cscope.sourceforge.net/cscope_vim_tutorial.html)
    *  [Vim Ctags](https://kulkarniamit.github.io/whatwhyhow/howto/use-vim-ctags.html)
    *  [Vim Gutentags](https://github.com/ludovicchabant/vim-gutentags)
  *  [Mdeval: Evaluate code blocks inside markdown](https://github.com/jubnzv/mdeval.nvim)
  *  [NeoVim GDB Plugin](https://github.com/sakhnik/nvim-gdb)
  *  [Vim-repl](https://github.com/sillybun/vim-repl)

## Tutorials

  * [Annotation Tutorial](https://alpha2phi.medium.com/neovim-for-beginners-code-annotation-and-documentation-4e8c1dd2552)
    
## Migrating

  * *Migrating Tutorial* 
    *  [Tutorial 1](https://otavio.dev/2018/09/30/migrating-from-vim-to-neovim/)
